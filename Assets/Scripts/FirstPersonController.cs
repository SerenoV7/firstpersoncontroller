using System.Collections;
using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class FirstPersonController : MonoBehaviour
{
    public bool CanMove { get; private set; } = true;
    private bool IsSprinting => canSprint && Input.GetKey(sprintKey);
    private bool ShouldJump => Input.GetKeyDown(jumpKey) && characterController.isGrounded;
    private bool ShouldCrouch => Input.GetKeyDown(crouchKey) && !duringCrouchAnimation && characterController.isGrounded;

    [Header("Functional Options")]
    [SerializeField] private bool canSprint = true;
    [SerializeField] private bool canJump = true;
    [SerializeField] private bool canCrouch = true;
    [SerializeField] private bool viewBobbing = true;
    [SerializeField] private bool hasTorch = true;
    [SerializeField] private bool WillSlideOnSlopes = true;
    [SerializeField] private bool canZoom = true;
    [SerializeField] private bool canInteract = true;
    [SerializeField] private bool useFootsteps = true;
    [SerializeField] private bool useStamina = true;
    [SerializeField] private bool useHealth = true;

    [Header("Controls")]
    [SerializeField] private KeyCode sprintKey = KeyCode.LeftShift;
    [SerializeField] private KeyCode jumpKey = KeyCode.Space;
    [SerializeField] private KeyCode crouchKey = KeyCode.LeftControl;
    [SerializeField] private KeyCode torchKey = KeyCode.F;
    [SerializeField] private KeyCode zoomKey = KeyCode.Mouse1;
    [SerializeField] private KeyCode interactKey = KeyCode.E;

    [Header("Movement Parameters")]
    [SerializeField] private float walkSpeed = 3.0f;
    [SerializeField] private float sprintSpeed = 6.0f;
    [SerializeField] private float crouchSpeed = 1.5f;
    [SerializeField] private float slopeSpeed = 8.0f;

    [Header("Jump Parameters")]
    [SerializeField] private float gravity = 30f;
    [SerializeField] private float jumpForce = 8.0f;

    [Header("Crouch Parameters")]
    [SerializeField] private float standingHeight = 2f;
    [SerializeField] private float crouchingHeight = 1.2f;
    [SerializeField] private float timeToCrouch = 0.25f;
    [SerializeField] private Vector3 crouchingCenter = new Vector3(0, 0.5f, 0);
    [SerializeField] private Vector3 standingCenter = new Vector3(0, 0, 0);
    private bool IsCrouching;
    private bool duringCrouchAnimation;

    [Header("Health Parameters")]
    [SerializeField] private float maxHealth = 100f;
    [SerializeField] private float timeBeforeRegen = 3f;
    [SerializeField] private float healthValueIncrement = 1f;
    [SerializeField] private float healthTimeIncrement = 0.1f;
    [SerializeField] private TextMeshProUGUI healthText = default;
    private float currentHealth;
    private Coroutine regenHealth;
    public static Action<float> OnTakeDamage;
    public static Action<float> OnDamage;
    public static Action<float> OnHeal;

    [Header("Stamina Parameters")]
    [SerializeField] private float maxStamina = 100f;
    [SerializeField] private float staminaUseMultiplier = 5f;
    [SerializeField] private float timeBeforeSprint = 5f;
    [SerializeField] private float staminaValueIncrement = 1f;
    [SerializeField] private float staminaTimeIncrement = 0.3f;
    [SerializeField] private float staminaUseJump = 10f;
    [SerializeField] private TextMeshProUGUI staminaText = default;
    private float currentStamina;
    private Coroutine regenStamina;
    public static Action<float> OnStaminaChange;

    [Header("Viewbob Parameters")]
    [SerializeField] private float walkBobSpeed = 14f;
    [SerializeField] private float walkBobAmount = 0.05f;
    [SerializeField] private float sprintBobSpeed = 18f;
    [SerializeField] private float sprintBobAmount = 0.075f;
    [SerializeField] private float crouchBobSpeed = 8f;
    [SerializeField] private float crouchBobAmount = 0.025f;
    private float defaultYPos = 0;
    private float timer;

    //SLIDING PARAMETERS
    private Vector3 hitPointNormal;
    private bool IsSliding
    {
        get 
        {
            if (characterController.isGrounded && Physics.Raycast(transform.position, Vector3.down, out RaycastHit slopeHit, 2f))
            {
                hitPointNormal = slopeHit.normal;
                return Vector3.Angle(hitPointNormal, Vector3.up) > characterController.slopeLimit;
            }
            else return false;
        }
    }

    [Header("Zoom Parameters")]
    [SerializeField] private float timeToZoom = 0.3f;
    [SerializeField] private float zoomFOV = 30.0f;
    private float defaultFOV;
    private Coroutine ZoomRoutine;

    [Header("Look Parameters")]
    [SerializeField, Range(1, 10)] private float lookSpeedX = 2.0f;
    [SerializeField, Range(1, 10)] private float lookSpeedY = 2.0f;
    [SerializeField, Range(1, 180)] private float upperLockLimit = 80.0f;
    [SerializeField, Range(1, 180)] private float lowerLockLimit = 80.0f;

    [Header("Interaction Parameters")]
    [SerializeField] private Vector3 interactionRayPoint = new Vector3(0.5f, 0.5f, 0.5f);
    [SerializeField] private float interactionDistance = 2f;
    [SerializeField] private LayerMask interactionLayer = default;
    private Interactable currentInterractable;

    [Header("Torch Parameters")]
    [SerializeField] private GameObject torchHolder = default;
    [SerializeField] private Color lightColor = new Color(255, 255, 255, 255);
    [SerializeField] private float lightRange = 60f;
    [SerializeField] private float lightAngle = 15f;
    [SerializeField] private float lightIntensity = 1f;
    private Light torch;

    [Header("Footstep Parameters")]
    [SerializeField] private float baseStepSpeed = 0.5f;
    [SerializeField] private float sprintStepMultiplier = 0.6f;
    [SerializeField] private float crouchStepMultiplier = 1.5f;
    [SerializeField] private AudioSource footstepAudioSource = default;
    [SerializeField] private AudioClip[] grassClips = default;
    [SerializeField] private AudioClip[] dirtClips = default;
    [SerializeField] private AudioClip[] gravelClips = default;
    [SerializeField] private AudioClip[] concreteClips = default;
    [SerializeField] private AudioClip[] carpetClips = default;
    [SerializeField] private AudioClip[] tilesClips = default;
    [SerializeField] private AudioClip[] waterClips = default;
    [SerializeField] private AudioClip[] woodClips = default;
    [SerializeField] private AudioClip[] snowClips = default;
    [SerializeField] private AudioClip[] floorClips = default;
    [SerializeField] private AudioClip sandClip = default;
    private float footstepTimer = 0f;
    private float GetCurrentOffset => IsCrouching ? baseStepSpeed * crouchStepMultiplier : IsSprinting ? baseStepSpeed * sprintStepMultiplier : baseStepSpeed;

    private Camera playerCamera;
    
    private CharacterController characterController;

    private Vector3 moveDirection;
    private Vector2 currentInput;

    private float rotationX = 0f;
    private bool torchStatus = false;

    private void OnEnable()
    {
        OnTakeDamage += ApplyDamage;
    }

    private void OnDisable()
    {
        OnTakeDamage -= ApplyDamage;
    }

    private void Awake()
    {
        playerCamera = GetComponentInChildren<Camera>();
        characterController = GetComponent<CharacterController>();
        defaultYPos = playerCamera.transform.localPosition.y;
        defaultFOV = playerCamera.fieldOfView;
        torch = torchHolder.GetComponent<Light>();
        Cursor.lockState = CursorLockMode.Locked;
        Cursor.visible = false;
        torch.enabled = torchStatus;
        torch.color = lightColor;
        torch.spotAngle = lightAngle;
        torch.intensity = lightIntensity;
        torch.range = lightRange;
        currentHealth = maxHealth;
        currentStamina = maxStamina;
        if(!useHealth) healthText.enabled = false;
        if(!useStamina) staminaText.enabled = false;
    }
    
    
    void Update()
    {
        if(CanMove)
        {
            HandleMovementInput();
            HandleMouseLook();
            ApplyFinalMovements();

            if (canJump) 
                HandleJump();

            if (canCrouch) 
                HandleCrouch();

            if (viewBobbing)
                HandleViewBobbing();

            if (hasTorch)
                HandleTorch();

            if (canZoom)
                HandleZoom();

            if (useFootsteps)
                HandleFootsteps();

            if (useStamina)
                HandleStamina();

            if (canInteract)
            {
                HandleInteractionCheck();
                HandleInteractionInput();
            }

        }
    }

    private void HandleMovementInput()
    {
        currentInput = new Vector2((IsCrouching ? crouchSpeed : IsSprinting ? sprintSpeed : walkSpeed) * Input.GetAxis("Vertical"), (IsCrouching ? crouchSpeed : IsSprinting ? sprintSpeed : walkSpeed) * Input.GetAxis("Horizontal"));
        float moveDirectionY = moveDirection.y;
        moveDirection = (transform.TransformDirection(Vector3.forward) * currentInput.x) + (transform.TransformDirection(Vector3.right) * currentInput.y);
        moveDirection.y = moveDirectionY;
    }

    private void HandleJump()
    {
        if (ShouldJump)
            if(useStamina)
                if(currentStamina >= staminaUseJump) 
                {
                    currentStamina -= staminaUseJump;
                    OnStaminaChange?.Invoke(currentStamina);
                    if(regenStamina != null)
                    {
                        StopCoroutine(regenStamina);
                        regenStamina = null;
                    }
                    moveDirection.y = jumpForce;
                    if(!IsSprinting && currentStamina < maxStamina && regenStamina == null)
                    {
                        regenStamina = StartCoroutine(RegenStamina());
                    }
                } else canJump = false;
            else moveDirection.y = jumpForce;
    }

    private void HandleCrouch()
    {
        if(ShouldCrouch) 
        {
            StartCoroutine(CrouchStand());
        }
    }

    private void ApplyDamage(float dmg)
    {
        if(!useHealth) return;
        currentHealth -= dmg;
        OnDamage?.Invoke(currentHealth);

        if(currentHealth <= 0) 
            KillPlayer();
        else if(regenHealth != null) 
            StopCoroutine(regenHealth);
        
        regenHealth = StartCoroutine(RegenHealth());
    }
    

    private void KillPlayer()
    {
        if(!useHealth) return;
        currentHealth = 0;

        if(regenHealth != null)
            StopCoroutine(regenHealth);

        print("DEAD AHAHAH LMAO");
    }

    private void HandleStamina()
    {
        if(IsSprinting && currentInput != Vector2.zero)
        {
            if(regenStamina != null)
            {
                StopCoroutine(regenStamina);
                regenStamina = null;
            }
            
            currentStamina -= staminaUseMultiplier * Time.deltaTime;

            if (currentStamina < 0) 
                currentStamina = 0;

            OnStaminaChange?.Invoke(currentStamina);

            if (currentStamina <= 0)
            {
                canSprint = false;
                canJump = false;
            }
               
        }   

        if(!IsSprinting && currentStamina < maxHealth && regenStamina == null)
        {
            regenStamina = StartCoroutine(RegenStamina());
        }
    }

    private void HandleZoom()
    {
        if (Input.GetKeyDown(zoomKey)) 
        {
            if (ZoomRoutine != null)
            {
                StopCoroutine(ZoomRoutine);
                ZoomRoutine = null;
            }
            ZoomRoutine = StartCoroutine(ToggleZoom(true));
        }
        if (Input.GetKeyUp(zoomKey)) 
        {
            if (ZoomRoutine != null)
            {
                StopCoroutine(ZoomRoutine);
                ZoomRoutine = null;
            }
            ZoomRoutine = StartCoroutine(ToggleZoom(false));
        }
    }

    private void HandleViewBobbing()
    {
        if (!characterController.isGrounded) return;

        if(Mathf.Abs(moveDirection.x) > 0.1f || Mathf.Abs(moveDirection.z) > 0.1f) 
        {
            timer += Time.deltaTime * (IsCrouching ? crouchBobSpeed : IsSprinting ? sprintBobSpeed : walkBobSpeed);
            playerCamera.transform.localPosition = new Vector3(
                playerCamera.transform.localPosition.x,
                defaultYPos + Mathf.Sin(timer) * (IsCrouching ? crouchBobAmount : IsSprinting ? sprintBobAmount : walkBobAmount),
                playerCamera.transform.localPosition.z);
        }
    }

    private void HandleInteractionCheck()
    {
        if (Physics.Raycast(playerCamera.ViewportPointToRay(interactionRayPoint), out RaycastHit hit, interactionDistance))
        {
            if(hit.collider.gameObject.layer == 3 && (currentInterractable == null || hit.collider.gameObject.GetInstanceID() != currentInterractable.gameObject.GetInstanceID()))
            {
                hit.collider.TryGetComponent(out currentInterractable);

                if(currentInterractable)
                    currentInterractable.OnFocus();
            }
        }
        else if (currentInterractable) 
        {
            currentInterractable.OnLoseFocus();
            currentInterractable = null;
        }
    }

    private void HandleInteractionInput()
    {
        if (Input.GetKeyDown(interactKey) && currentInterractable != null && Physics.Raycast(playerCamera.ViewportPointToRay(interactionRayPoint), out RaycastHit hit, interactionDistance, interactionLayer))
        {
            currentInterractable.OnInteract();
        }
    }

    private void HandleTorch()
    {
        torchHolder.transform.rotation = playerCamera.transform.rotation;
        if (Input.GetKeyDown(torchKey))
        {
            torchStatus = !torchStatus;
            torch.enabled = torchStatus;
        }
    }

    private void HandleFootsteps()
    {
        if (!characterController.isGrounded) return;
        if (currentInput == Vector2.zero) return;
       
        footstepTimer -= Time.deltaTime;

        if (footstepTimer <= 0) 
        {
            if(Physics.Raycast(characterController.transform.position, Vector3.down, out RaycastHit hit, 3))
            {
                switch(hit.collider.tag)
                {
                    case "Footsteps/GRASS":
                        footstepAudioSource.PlayOneShot(grassClips[UnityEngine.Random.Range(0, grassClips.Length - 1)]);
                        break;
                    case "Footsteps/DIRT":
                        footstepAudioSource.PlayOneShot(dirtClips[UnityEngine.Random.Range(0, dirtClips.Length - 1)]);
                        break;
                    case "Footsteps/GRAVEL":
                        footstepAudioSource.PlayOneShot(gravelClips[UnityEngine.Random.Range(0, gravelClips.Length - 1)]);
                        break;
                    case "Footsteps/CONCRETE":
                        footstepAudioSource.PlayOneShot(concreteClips[UnityEngine.Random.Range(0, concreteClips.Length - 1)]);
                        break;
                    case "Footsteps/SAND":
                        footstepAudioSource.PlayOneShot(sandClip);
                        break;
                    case "Footsteps/SNOW":
                        footstepAudioSource.PlayOneShot(snowClips[UnityEngine.Random.Range(0, snowClips.Length - 1)]);
                        break;
                    case "Footsteps/CARPET":
                        footstepAudioSource.PlayOneShot(carpetClips[UnityEngine.Random.Range(0, carpetClips.Length - 1)]);
                        break;
                    case "Footsteps/WATER":
                        footstepAudioSource.PlayOneShot(waterClips[UnityEngine.Random.Range(0, waterClips.Length - 1)]);
                        break;
                    case "Footsteps/TILES":
                        footstepAudioSource.PlayOneShot(tilesClips[UnityEngine.Random.Range(0, tilesClips.Length - 1)]);
                        break;
                    case "Footsteps/WOOD":
                        footstepAudioSource.PlayOneShot(woodClips[UnityEngine.Random.Range(0, woodClips.Length - 1)]);
                        break;
                    case "Footsteps/FLOOR":
                    default:
                        footstepAudioSource.PlayOneShot(floorClips[UnityEngine.Random.Range(0, floorClips.Length - 1)]);
                        break;
                }
            }

            footstepTimer = GetCurrentOffset;
        }
    }

    private void HandleMouseLook()
    {
        rotationX -= Input.GetAxis("Mouse Y") * lookSpeedY;
        rotationX = Mathf.Clamp(rotationX, -upperLockLimit, lowerLockLimit);
        playerCamera.transform.localRotation = Quaternion.Euler(rotationX, 0, 0);

        transform.rotation *= Quaternion.Euler(0, Input.GetAxis("Mouse X") * lookSpeedX, 0);
    }

    private void ApplyFinalMovements()
    {
        if (!characterController.isGrounded) 
            moveDirection.y -= gravity * Time.deltaTime;

        if (WillSlideOnSlopes && IsSliding)
            moveDirection += new Vector3(hitPointNormal.x, -hitPointNormal.y, hitPointNormal.z) * slopeSpeed;

        characterController.Move(moveDirection * Time.deltaTime);
    }

    private IEnumerator CrouchStand()
    {
        if (IsCrouching && Physics.Raycast(playerCamera.transform.position, Vector3.up, 1f)) yield break;

        duringCrouchAnimation = true;

        float timeElapsed = 0;
        float targetHeight = IsCrouching ? standingHeight : crouchingHeight;
        float currentHeight = characterController.height;
        Vector3 targetCenter = IsCrouching ? standingCenter : crouchingCenter;
        Vector3 currentCenter = characterController.center;

        while (timeElapsed < timeToCrouch) 
        {
            characterController.height = Mathf.Lerp(currentHeight, targetHeight, timeElapsed / timeToCrouch);
            characterController.center = Vector3.Lerp(currentCenter, targetCenter, timeElapsed / timeToCrouch);
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        characterController.height = targetHeight;
        characterController.center = targetCenter;

        IsCrouching = !IsCrouching;

        duringCrouchAnimation = false;
    }

    private IEnumerator ToggleZoom(bool isEnter)
    {
        float targetFOV = isEnter ? zoomFOV : defaultFOV;
        float startingFOV = playerCamera.fieldOfView;
        float timeElapsed = 0f;

        while(timeElapsed < timeToZoom)
        {
            playerCamera.fieldOfView = Mathf.Lerp(startingFOV, targetFOV, timeElapsed / timeToZoom);
            timeElapsed += Time.deltaTime;
            yield return null;
        }

        playerCamera.fieldOfView = targetFOV;
        ZoomRoutine = null;
    }

    private IEnumerator RegenHealth()
    {
        yield return new WaitForSeconds(timeBeforeRegen);
        WaitForSeconds timeToWait = new WaitForSeconds(healthTimeIncrement);
        while (currentHealth < maxHealth)
        {
            currentHealth += healthValueIncrement;

            if(currentHealth > maxHealth)
                currentHealth = maxHealth;

            OnHeal?.Invoke(currentHealth);
            yield return timeToWait;
        }
        regenHealth = null;
    }

    private IEnumerator RegenStamina()
    {
        yield return new WaitForSeconds(timeBeforeSprint);
        WaitForSeconds timeToWait = new WaitForSeconds(staminaTimeIncrement);
        while(currentStamina < maxStamina)
        {
            if(currentStamina > 0)
            {
                canSprint = true;
                if(currentStamina >= 10) canJump = true;
            } 
            currentStamina += staminaValueIncrement;

            if (currentStamina > maxStamina)
                currentStamina = maxStamina;

            OnStaminaChange?.Invoke(currentStamina);

            yield return timeToWait;
        }

        regenStamina = null;
    }
}
