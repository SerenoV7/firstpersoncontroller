using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleInteractable : Interactable
{
    public override void OnFocus()
    {
        print("FOCUS GAINED");
    }

    public override void OnInteract()
    {
        print("INTERACTED");
    }

    public override void OnLoseFocus()
    {
        print("FOCUS LOST");
    }
}
